.686
.MODEL FLAT
.CODE
public _DecToHex
_DecToHex PROC
push ebp
mov ebp, esp
push edi
push esi
mov eax, [ebp + 8]; number
mov edi, [ebp + 12]; string
xor esi, esi; str index

mov ebx, 16
division:
	cdq
	idiv ebx 
	; transform edx into hex
	; +8? 
	cmp edx, 10
	jge mvLetter
	push eax
	xor eax, eax
	mov al , '0'
	add al, dl
	mov [edi + esi], al
	pop eax
	jmp contin

	mvLetter:
	push eax

	mov eax, edx
	sub eax, 10
	mov edx, eax

	mov eax, '9'
	add eax , 8
	add eax, edx

	mov [edi + esi], al
	pop eax

	contin:
	inc esi
	cmp eax, 0
jne division

; reverse order
;inc esi; len
mov ecx, esi
dec esi
shr ecx,1; repeats
xor edx ,edx
rev:
mov al, BYTE ptr [edi + esi]
mov bl, BYTE ptr [edi + edx]

mov byte ptr [edi + edx], al
mov byte ptr [edi + esi], bl
dec esi
inc edx
loop rev

mov eax, edi

pop esi
pop edi
pop ebp
ret
_DecToHex ENDP
END