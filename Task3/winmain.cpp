#undef UNICODE
#define _CRT_SECURE_NO_WARNINGS
#define IDD_DIALOG 101 
#define ID_OUTPUT 1001 
#define ID_INPUT 1002 
#define ID_CONFIRM 1003 
#define ID_EXIT 1004 
#define MAX_BUFF 1024
#include <windows.h>
#include  <math.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
const char* _projectName = "DecToHex";
char* reverse(char* arr);
extern "C" void _cdecl DecToHex(int number, char* out);
BOOL InitWnd(HINSTANCE hinstance);
BOOL InitInstance(HINSTANCE hinstance, int nCmdShow);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
BOOL CALLBACK ProcHandler(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE prevHinstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;

	if (!InitWnd(hinstance))
	{
		MessageBox(NULL, "Unable to Init App", "Error", MB_OK);
		return FALSE;
	}

	if (!InitInstance(hinstance, nCmdShow))
	{
		MessageBox(NULL, "Unable to Init Instance", "Error", MB_OK);
		return FALSE;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

BOOL InitWnd(HINSTANCE hinstance)
{
	WNDCLASSEX wndclass;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hinstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_CROSS);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = _projectName;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wndclass))
	{
		MessageBox(NULL, "Cannot register class", "Error", MB_OK);
		return FALSE;
	}
	return TRUE;
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	static int x, y;
	static int returnCode;
	static HDC hdc;
	static const char* retCodeStr;
	PAINTSTRUCT ps;
	switch (message)
	{
	case WM_CREATE:
		returnCode = DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG), hwnd, ProcHandler);
		SendMessage(hwnd, WM_DESTROY, 0, 0);
		break;
	case WM_SIZE:
		x = LOWORD(lparam);
		y = HIWORD(lparam);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);

		EndPaint(hwnd, &ps);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, message, wparam, lparam);
	}
	return FALSE;
}

BOOL CALLBACK ProcHandler(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	static int numberTohandle;
	static char buff[MAX_BUFF];
	switch (message)
	{
	case WM_INITDIALOG:
		numberTohandle = 0;
		break;
	case WM_COMMAND:
		switch (LOWORD(wparam))
		{
		case ID_CONFIRM:
			GetWindowText(GetDlgItem(hwnd, ID_INPUT), buff, MAX_BUFF);
			numberTohandle = atoi(buff);
			fill(buff, buff + MAX_BUFF, 0); // reset buff

			DecToHex(numberTohandle, buff);
			SetWindowText(GetDlgItem(hwnd, ID_OUTPUT), buff);
			// handle number
			return FALSE;
		case ID_EXIT:
			EndDialog(hwnd, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}

BOOL InitInstance(HINSTANCE hinstance, int nCmdShow)
{
	HWND hwnd;
	hwnd = CreateWindow(
		_projectName,
		_projectName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		0,
		CW_USEDEFAULT,
		0,
		NULL,
		NULL,
		hinstance,
		NULL);


	if (!hwnd)
		return FALSE;
	//ShowWindow(hwnd, nCmdShow);
	//UpdateWindow(hwnd);
	return TRUE;
}