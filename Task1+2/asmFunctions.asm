.586
.MODEL FLAT
.DATA
strL dd 0
filterL dd 0
resStr db 255 dup(0)
startAddr dd 0
.CODE 
public _getWord
_getWord PROC
push ebp
mov ebp ,esp
push esi

mov esi, [ebp + 8]; string
mov edi, [ebp + 12]; filter
mov startAddr, edi
mov ebx,  [ebp + 16]
mov strL, ebx; string length
mov ebx , [ebp + 20]
mov filterL, ebx; filter string len
; -------------<reading data>------------------
cmp strL,0
jg find
mov eax, 0
jmp finish_func

find:xor edx, edx ; current string index 
nextSymbol: 
; Handle [i] element
mov al, BYTE PTR [esi + edx]; element to look for
mov ecx, filterL
mov edi, startAddr
repne scasb 
jecxz not_found
jmp found 

not_found:
inc edx
cmp edx, strL
jl nextSymbol
jmp contin

found:
; Copy from the beginning 

mov ecx, (SIZEOF resStr)
mov al, 0
lea edi, resStr
rep stosb

cmp edx, 0
jg copy_str
mov [resStr], ' '

copy_str:
mov ecx, edx
lea edi, resStr
rep movsb
lea eax, resStr
jmp finish_func 

contin:
lea eax, resStr

finish_func:
pop esi
pop ebp
ret
_getWord ENDP
END