#undef UNICODE
#define _CRT_SECURE_NO_WARNINGS
#define IDD_DIALOG 101
#define ID_OPENBUTTON 1002
#define ID_LIST 1003
#include <windows.h>
#include  <math.h>
#include <string>
#include <sstream>

using namespace std;
const char* _projectName = "OpenFileDialogApp";
const char* separators = ".\\_- : ";

extern "C" char* _cdecl getWord(const char* string , const char* filter, int strLen, int filterLen);
BOOL InitWnd(HINSTANCE hinstance);
BOOL InitInstance(HINSTANCE hinstance, int nCmdShow);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
BOOL CALLBACK OpenHandler(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE prevHinstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;

	if (!InitWnd(hinstance))
	{
		MessageBox(NULL, "Unable to Init App", "Error", MB_OK);
		return FALSE;
	}

	if (!InitInstance(hinstance, nCmdShow))
	{
		MessageBox(NULL, "Unable to Init Instance", "Error", MB_OK);
		return FALSE;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

BOOL InitWnd(HINSTANCE hinstance)
{
	WNDCLASSEX wndclass;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hinstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_CROSS);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = _projectName;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wndclass))
	{
		MessageBox(NULL, "Cannot register class", "Error", MB_OK);
		return FALSE;
	}
	return TRUE;
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	static int x, y;
	static int returnCode;
	static HDC hdc;
	static const char* retCodeStr;
	PAINTSTRUCT ps;
	switch (message)
	{
	case WM_CREATE:
		returnCode = DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_DIALOG), hwnd, OpenHandler);
		retCodeStr = (returnCode) ? "Button OK" : "Button Cancel";
		MessageBox(NULL, retCodeStr, "Message", MB_OK);
		SendMessage(hwnd, WM_CLOSE, 0, 0);
		break;
	case WM_SIZE:
		x = LOWORD(lparam);
		y = HIWORD(lparam);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		
		EndPaint(hwnd, &ps);
		break;
	
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, message, wparam, lparam);
	}
	return FALSE;
}

BOOL CALLBACK OpenHandler(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	static OPENFILENAME ofn;
	static char fn[MAX_PATH];
	static HWND list;
	switch (message)
	{
	case WM_INITDIALOG:
		list = GetDlgItem(hwnd, ID_LIST);
		break;
	case WM_COMMAND:
		switch(LOWORD(wparam))
		{
		case ID_OPENBUTTON:
			ZeroMemory(&ofn, sizeof(ofn));
			ZeroMemory(&fn, sizeof(fn));
			ofn.lStructSize = sizeof(OPENFILENAME);
			ofn.hwndOwner = hwnd;
			ofn.lpstrFile = fn;
			ofn.lpstrFilter = "Text Files\0*.txt\0Any File\0*.*\0";
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrTitle = "Select a file:";
			

			if (GetOpenFileName(&ofn))
			{
				string buff;

				strcpy_s(fn, ofn.lpstrFile);
				buff = fn;

				char* substr = strtok(fn,separators);
				while (substr != NULL)
				{
					SendMessage(list, LB_ADDSTRING, 0, (LPARAM)substr);
					substr = strtok(NULL, separators);
				}
				SendMessage(list, LB_ADDSTRING, 0, (LPARAM)buff.c_str());

				// using assembly fubction
				buff += " ";
				string out;
				substr = getWord(buff.data(), separators, buff.size(), strlen(separators));
				char* ptr = &buff[0];
				while(substr != NULL)
				{
					ptr += strlen(substr);
					out += (substr[0] != ' ')?string(substr) + "\r\n":"";
					substr = getWord(ptr, separators, strlen(ptr), strlen(separators));
				}
				MessageBox(NULL, out.data(), "MSG", MB_OK);
			}
			break;
		case IDOK:
			EndDialog(hwnd, 1);
			return FALSE;
		case IDCANCEL:
			EndDialog(hwnd, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}

BOOL InitInstance(HINSTANCE hinstance, int nCmdShow)
{
	HWND hwnd;
	hwnd = CreateWindow(
		_projectName,
		_projectName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		0,
		CW_USEDEFAULT,
		0,
		NULL,
		NULL,
		hinstance,
		NULL);


	if (!hwnd)
		return FALSE;
	//ShowWindow(hwnd, nCmdShow);
	//UpdateWindow(hwnd);
	return TRUE;
}